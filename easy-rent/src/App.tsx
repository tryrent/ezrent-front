import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import { StateProvider } from './State/AppStateProvider';

import Page from './Layout/Page';
import Home from './Pages/Home';

const App: React.FC = () => (
    <StateProvider>
        <Router>
            <Switch>
              <Route exact path='/'>
                <Page><Home /></Page>
              </Route>
            </Switch>
        </Router>
    </StateProvider>
);

export default App;

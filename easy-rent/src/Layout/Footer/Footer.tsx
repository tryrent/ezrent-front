import React from 'react';

import Container from '@material-ui/core/Container';

import useStyles from './styles';

const Footer: React.FC = () => {
    const classes = useStyles();

    return (
        <footer className={classes.root}>
            <Container maxWidth="lg">
                All rights reserved, 2020
            </Container>
        </footer>
    )
};

export default Footer;
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => createStyles({
    root: {
        backgroundColor: '#6c6c6c'
    }
}), {
    name: 'Footer'
});

export default useStyles;
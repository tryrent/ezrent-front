import React from 'react';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import AccountCircleIcon from '@material-ui/icons/AccountCircleOutlined'

import useStyles from './styles';

const Header: React.FC = () => {
    const classes = useStyles();

    return (
        <AppBar position="static">
            <Toolbar>
                <Typography className={classes.title}>Easy Rent</Typography>
                <AccountCircleIcon />
            </Toolbar>
        </AppBar>
    );
};

export default Header;
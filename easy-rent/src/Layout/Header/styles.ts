import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => createStyles({
    root: {},
    title: {
        flexGrow: 1,
    }
}), {
    name: 'Header'
});

export default useStyles;
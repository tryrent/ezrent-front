import React from 'react';

import Header from '../Header';
import Footer from '../Footer';

import Container from '@material-ui/core/Container';

import useStyles from './styles';

type Props = {
    children: React.ReactNode
}

const Page: React.FC<Props> = ({ children }) => (
    <>
        <Header />
        <main> 
            <Container maxWidth="lg">
                { children }
            </Container>
        </main>
        <Footer />
    </>
);

export default Page;
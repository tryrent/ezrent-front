import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => createStyles({
    root: {}
}), {
    name: 'Page'
});

export default useStyles;
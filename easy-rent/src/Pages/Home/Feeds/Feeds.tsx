import React from 'react';

import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import useStyles from './styles';

const Feeds: React.FC = () => {
    const classes = useStyles();

    return (
        <Grid container>
            <Grid item xs={12} md={6}>
                <Card className={classes.card}>
                    <CardHeader
                        title={"Feed1"}
                        subheader={"Here goes the description"}
                    />
                    <div className={classes.image} />
                    <CardContent>
                        <Typography variant="body1">Sample text...</Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={12} md={6}>
                <Card className={classes.card}>
                    <CardHeader
                        title={"Feed2"}
                        subheader={"Here goes the description"}
                    />
                    <div className={classes.image} />
                    <CardContent>
                        <Typography variant="body1">Sample text...</Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={12} md={6}>
                <Card className={classes.card}>
                    <CardHeader
                        title={"Feed3"}
                        subheader={"Here goes the description"}
                    />
                    <div className={classes.image} />
                    <CardContent>
                        <Typography variant="body1">Sample text...</Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={12} md={6}>
                <Card className={classes.card}>
                    <CardHeader
                        title={"Feed4"}
                        subheader={"Here goes the description"}
                    />
                    <div className={classes.image} />
                    <CardContent>
                        <Typography variant="body1">Sample text...</Typography>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    )
};

export default Feeds;
import React from 'react';
import { observer } from 'mobx-react';

import Search from './Search';
import Feeds from './Feeds';

import useStyles from './styles';
import { useAppState } from '../../State/AppStateProvider';

const Home: React.FC<{}> = () => {
    const classes = useStyles();

    const { theme }= useAppState();

    return (
        <div className={classes.root}>
            <Search />
            <Feeds />
        </div>
    );
};

export default observer(Home);
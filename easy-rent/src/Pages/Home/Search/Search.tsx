import React from 'react';

import InputBase from '@material-ui/core/InputBase';

import Typography from '@material-ui/core/Typography';

import SearchIcon from '@material-ui/icons/SearchOutlined';

import useStyles from './styles';

const Search: React.FC = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Typography variant="h5">Find a blog, site or feed</Typography>
            <Typography variant="subtitle1">We'll automatically post new items for you</Typography>
            <div className={classes.search}>
                <div className={classes.searchIcon}>
                <SearchIcon />
                </div>
                <InputBase
                placeholder="Search…"
                classes={{
                    input: classes.inputInput,
                }}
                inputProps={{ 'aria-label': 'search' }}
                />
            </div>
        </div>
    );
};

export default Search;
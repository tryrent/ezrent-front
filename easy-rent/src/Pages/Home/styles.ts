import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => createStyles({
    root: {
        backgroundColor: '#f0f6f7'
    },
}), {
    name: 'Home'
});

export default useStyles;
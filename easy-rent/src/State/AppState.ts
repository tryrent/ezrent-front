import { observable } from 'mobx';

class AppState {
    @observable theme = "default";
};

export default new AppState();
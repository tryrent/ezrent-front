import React, { createContext, useContext } from 'react';

import AppState from './AppState';

export const AppContext = createContext(AppState);

export const StateProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
    return (
        <AppContext.Provider value={AppState}>
            {children}
        </AppContext.Provider>
    );
};

export const useAppState = () => useContext(AppContext);